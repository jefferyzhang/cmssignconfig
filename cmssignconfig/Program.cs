﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using cmssignconfig.Properties;
using CertificatesiDevice;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Diagnostics;
using System.IO;
using System.Runtime.ExceptionServices;

namespace cmssignconfig
{
    class Program
    {
        static string tag = "";
        static public void logIt(string s, bool outputStdout = true, bool outputDebugger = true)
        {
            if (outputStdout) System.Console.WriteLine(s);
            if (outputDebugger)
            {
                System.Diagnostics.Trace.WriteLine(s);
                if (!string.IsNullOrEmpty(tag))
                {
                    System.Diagnostics.Trace.WriteLine(string.Format("[{0}]: {1}", tag, s));
                }
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            logIt(e.ToString());
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        [HandleProcessCorruptedStateExceptionsAttribute]
        static int Main(string[] args)
        {
            string sInFileName="";
            string sOutFileName="";

            System.Configuration.Install.InstallContext _param = new System.Configuration.Install.InstallContext(null, args);
            if (_param.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for attach, press any key to continue...");
                System.Console.ReadKey();
            }
            System.AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            if (_param.Parameters.ContainsKey("in"))
            {
                sInFileName = _param.Parameters["in"];
            }
            if(_param.Parameters.ContainsKey("out"))
            {
                sOutFileName = _param.Parameters["out"];
            }


            if (string.IsNullOrEmpty(sOutFileName))
            {
                logIt("not output file name!");
                return 1;
            }
            if (string.Compare(sInFileName, "GetCertificate", true) == 0)
            {
                try
                {
                    FileStream fileStream = new FileStream(sOutFileName, FileMode.Create);
                    fileStream.Write(Resources.Certifcate, 0, Resources.Certifcate.Length);
                    fileStream.Close();
                }
                catch (Exception exception)
                {
                    Trace.WriteLine(exception.ToString());
                    return 5;
                }
                return 0;
            }
            if(string.IsNullOrEmpty(sInFileName)||!File.Exists(sInFileName))
            {
                logIt("input file not exist!");
                return 2;
            }

            X509Certificate2 certificate = new X509Certificate2(Resources.Certifcate);
            string sPrivateKey = Encoding.UTF8.GetString(Resources.PrivateKey);
            byte[] keyBuffer = Helpers.GetBytesFromPEM(sPrivateKey, PemStringType.RsaPrivateKey);

            RSACryptoServiceProvider prov = Crypto.DecodeRsaPrivateKey(keyBuffer);
            certificate.PrivateKey = prov;

            Oid digestOid = new Oid("1.2.840.113549.1.7.1");


            try
            {
                ContentInfo contentInfo = new ContentInfo(digestOid, File.ReadAllBytes(sInFileName));

                SignedCms signedCMS = new SignedCms(contentInfo, false);

                CmsSigner cmsSigner = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, certificate);

                // Oid signedDate = new Oid("1.2.840.113549.1.9.5"); //oid for PKCS #9 signing time 
                cmsSigner.DigestAlgorithm = new Oid("1.3.14.3.2.26");//sha1
                signedCMS.ComputeSignature(cmsSigner);
                byte[] encoded = signedCMS.Encode();

                try
                {
                    FileStream fileStream = new FileStream(sOutFileName, FileMode.Create);
                    fileStream.Write(encoded, 0, encoded.Length);
                    fileStream.Close();
                }
                catch (Exception exception)
                {
                    Trace.WriteLine(exception.ToString());
                    return 4;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                return 3;
            }

            return 0;

        }
    }
}
